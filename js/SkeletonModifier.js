class SkeletonModifier extends Modifier {

    constructor() {
        super();
        this.transforms = [];
    }

    apply(target) {
        if (!SkeletonModifier.isValid(target)) return;

        for (let transform of this.transforms) {
            transform(target);
        }
    }

    scaleBone(name, scale, scaleChildren = true) {
        this.transforms.push(function(root) {
            let bone = root.getObjectByName(name);
            let scaleMatrix = new THREE.Matrix4().makeScale(scale.x, scale.y, scale.z);

            bone.applyMatrix(scaleMatrix);

            if (!scaleChildren) {
                let inverseMatrix = new THREE.Matrix4().makeScale(1/scale.x, 1/scale.y, 1/scale.z);

                for (let children of bone.children) {
                    children.applyMatrix(inverseMatrix);
                }
            }
        });
    }

    translateBone(name, translation) {
        this.transforms.push(function(root) {
            let bone = root.getObjectByName(name);
            let translationMatrix = new THREE.Matrix4().makeTranslation(translation.x, translation.y, translation.z);
            bone.applyMatrix(translationMatrix);
        });
    }

    static isValid(target) {
        if (!target || !target.isBone) {
            console.error("SkeletonModifier: cannot apply modifier (target is not a bone)");
            return false;
        }

        return true;
    }

}